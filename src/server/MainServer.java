package server;

import java.net.DatagramSocket;
import java.net.InetSocketAddress;
import java.net.SocketException;
import java.util.concurrent.ConcurrentHashMap;

/**
 * 
 * @author Illum
 * @since 31-March-2014
 */
public class MainServer
{
	private int port;
	private DatagramSocket socket;
	private ConcurrentHashMap<InetSocketAddress, ClientData> joinedClients;
	private Pulser pulseCheck;
	private Communication messageReceiver;
	
	// protocol keyword
	// server -> client
//	PING_KEYWORD = "PING";
//	JOIN_ERROR_KEYWORD = "JERR";
//	JOIN_SUCCES_KEYWORD = "JOK";
//	MESSAGE_KEYWORD = "MSG";
	// client -> server
//	JOIN_KEYWORD = "JOIN";
//	EXIT_KEYWORD = "EXIT";
//	MSGA_KEYWORD = "MSGA";
//	MSGP_KEYWORD = "MSGP";
//	PONG_KEYWORD = "PONG";
	
	
	public static void main(String[] args)
	{
		new MainServer();
	}
	
	
	private MainServer()
	{
		joinedClients = new ConcurrentHashMap<InetSocketAddress, ClientData>();
		port = 7500;
		try
		{
			socket = new DatagramSocket(port);
			messageReceiver = new Communication(joinedClients, socket);
			pulseCheck = new Pulser(joinedClients, socket);
			Thread messageReceiverThread = new Thread(messageReceiver);
			Thread pulserThread = new Thread(pulseCheck);
			messageReceiverThread.start();
			pulserThread.start();
			
			// To see updates in quicker intervals in order to debug
			Thread showMeConsoleUpdates = new Thread(new ShowDataThingy(joinedClients));
			showMeConsoleUpdates.start();
		}
		catch (SocketException e)
		{
			System.out.println("Server failed to start up due to socket error");
			e.printStackTrace();
		}	
	}
	
	
}
