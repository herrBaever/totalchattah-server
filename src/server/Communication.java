package server;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetSocketAddress;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.regex.Pattern;

/**
 * 
 * @author Illum
 * @since 31-March-2014
 */
public class Communication implements Runnable
{
	private ConcurrentHashMap<InetSocketAddress, ClientData> joinedClients;
	private DatagramSocket socket;
	private boolean receiveActive;
	private String incMessage;
	private String[] splitMessage;
	private Pattern whiteSpaceSplitter;
	private final int PACKETSIZE = 400;
	
	
	protected Communication(ConcurrentHashMap<InetSocketAddress, ClientData> joinedClients, DatagramSocket socket)
	{
		receiveActive = true;
		this.joinedClients = joinedClients;
		this.socket = socket;
		// Pattern for split delimiter as regex - "\\s+": whitespace that occurs at least one time.
		whiteSpaceSplitter = Pattern.compile("\\s+");
	}
	
	
	public void run()
	{
		while (receiveActive)
		{
			receive();
		}
	}
	
	
	private void receive()
	{
		try
		{
			DatagramPacket packetFromClient = new DatagramPacket(new byte[PACKETSIZE], PACKETSIZE);
			socket.receive(packetFromClient);
			// clientPacket now contains message data
			incMessage = new String(packetFromClient.getData()).trim();	//Any way to instead dynamically adjust packet size so it doesn't include trailing whitespaces?			
			System.out.println("Raw trimmed message: [" + incMessage + "] " + "from " + packetFromClient.getAddress() + ":" + packetFromClient.getPort());
			// TODO Update code to be compatible with Java 8 which brought a change to Pattern.split.
			splitMessage = whiteSpaceSplitter.split(incMessage, 2);	// Up to 2 strings around the split
			handleMessage((InetSocketAddress) packetFromClient.getSocketAddress());			
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
	}
	
	
	// TODO Should remote port be include as parameter to more uniquely identify client?
	private void handleMessage(InetSocketAddress clientAddress) throws IOException
	{
		// TODO Is this null test necessary? Consider this method is run very frequently.
		if (splitMessage != null)
		{
			if (joinedClients.containsKey(clientAddress))
			{
				switch(splitMessage[0])
				{
				case "PONG" : pong(clientAddress); break;
				case "MSGA" : messageAll(clientAddress); break;
				case "MSGP" : messagePrivately(clientAddress); break;
				case "EXIT" : exit(clientAddress); break;
//				case "JOIN" : break;	// TODO (How) should JOIN be handled if client is already joined?
				default : System.out.println("Invalid Request by already joined client: " + splitMessage[0]);
				}
			}
			// If client isn't joined:
			else if(splitMessage[0].equals("JOIN"))
			{
				join(clientAddress); 
			}
			else
			{
				System.out.println("Invalid Request by client not joined: " + splitMessage[0]);
			}
		}
		else
		{
			System.out.println("ARGGG HOW DID AN EMPTY REQUEST GET HERE?!");
		}
	}
	
	
	private void pong(InetSocketAddress clientAddress)
	{
		/* TODO Should one test if longer than just the "PONG" or disregard?
		 It would be easy to test: If splitMessage array has more than one element
		But it would mean another test and pong() is run quite frequently */
		joinedClients.get(clientAddress).resetPulsesMissed();
	}
	
	
	private void messageAll(InetSocketAddress senderAddress) throws IOException
	{
		// If request has message and this message is at least 1 length
		if (splitMessage.length > 1 && splitMessage[1].length() > 0)
		{
			byte[] msgBytes = ("MSG" + whoIsNickAndThenWrap(senderAddress) + splitMessage[1]).getBytes();
			DatagramPacket massMsg = new DatagramPacket(msgBytes, msgBytes.length);
			for (Map.Entry<InetSocketAddress, ClientData> entry : joinedClients.entrySet())
			{
				massMsg.setSocketAddress(entry.getKey());
				socket.send(massMsg);
			}
			System.out.println(new String(msgBytes));
		}
	}
	
	
	// TODO Should one receive ones own message from server?
	// TODO And couldn't I find a smarter way than to iterate through all the values of the map?
	private void messagePrivately(InetSocketAddress senderAddress) throws IOException
	{	
		// This request must have more than one token. Array should consist of 2
		// elements at this point. {KEYWORD, rest}
		if (splitMessage.length == 2  && splitMessage[1].charAt(0) == '(' )
		{
			int indexOfNickEnd = splitMessage[1].indexOf(')' );
			String receivingNick = splitMessage[1].substring(0, indexOfNickEnd + 1);
			System.out.println("debug other nick: " + receivingNick);
			for (ClientData clientData: joinedClients.values())
			{
				// Find the ip of the other receiving client if it exists
				if (clientData.getNick().equals(receivingNick))
				{
					InetSocketAddress receivingIP = clientData.getAddress();
					// A matching IP exists and there's a message of at least 1 char length
					if (receivingIP != null && splitMessage[1].length() > receivingNick.length() + 1)
					{
						// Position just after ')' is a white space, so shift two positions
						byte[] msgBytesToReceiver = ("MSG" + whoIsNickAndThenWrap(senderAddress)
															+ splitMessage[1].substring(indexOfNickEnd + 1 + 1)).getBytes();	
						System.out.println("debug message: " + new String(msgBytesToReceiver));
						DatagramPacket privateMsg = new DatagramPacket(msgBytesToReceiver, msgBytesToReceiver.length,
																						receivingIP);
						socket.send(privateMsg);
						// Also transmitting to sending client
						privateMsg.setSocketAddress(senderAddress);
						socket.send(privateMsg);
					}		
					break;	// no need to finish iterating if there has been a match
				}
			}

		}

	}
	
	
	private void join(InetSocketAddress clientAddress) throws IOException
	{
		String joinStatusMsg;
		// If message has data after join keyword
		if (splitMessage.length > 1 && isNameAvailable(splitMessage[1]))
		{
			// Add new clientData to joinedClients
			joinedClients.put(clientAddress, new ClientData(clientAddress, splitMessage[1]));
			joinStatusMsg = "JOK";
		}
		else
		{
			if (splitMessage.length == 1)
			{
				joinStatusMsg = "JERR no nick received";
			}
			else
			{
				joinStatusMsg = "JERR name taken";
			}
		}
		DatagramPacket joinStatusPacket = new DatagramPacket(joinStatusMsg.getBytes(),
														joinStatusMsg.length(), clientAddress);
		socket.send(joinStatusPacket);
		System.out.println(joinStatusMsg);
	}
	
	
	private void exit(InetSocketAddress clientAddress)
	{
		// removing client
		joinedClients.remove(clientAddress);
	}
	
	
	private boolean isNameAvailable(String joiningNick)
	{
		for (ClientData clientData : joinedClients.values())
		{
			if (clientData.getNick().equals(joiningNick))
			{
				return false;
			}
		}
		return true;
	}
	
	
	private String whoIsNickAndThenWrap(InetSocketAddress clientAddress)
	{
		return " " + joinedClients.get(clientAddress).getNick() + " ";
	}
	
	
	protected void setReceiveActive(boolean receiveActive)
	{
		this.receiveActive = receiveActive;
	}
	

}
