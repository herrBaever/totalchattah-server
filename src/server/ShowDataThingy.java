package server;

import java.net.InetSocketAddress;
import java.util.concurrent.ConcurrentHashMap;

public class ShowDataThingy implements Runnable
{

	
	private ConcurrentHashMap<InetSocketAddress, ClientData> joinedClients;
	
	public ShowDataThingy(ConcurrentHashMap<InetSocketAddress, ClientData> joinedClients)
	{
		this.joinedClients = joinedClients;
	}
	
	
	@Override
	public void run()
	{
		while (true)
		{
			System.out.println(joinedClients + " " + joinedClients.size());
			try
			{
				Thread.sleep(1000);
			}
			catch (InterruptedException e)
			{
				e.printStackTrace();
			}
		}
	}

	
}
