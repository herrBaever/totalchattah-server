package server;

import java.net.InetSocketAddress;

/**
 * 
 * @author Illum
 * @since 31-March-2014
 */
public class ClientData /*implements Comparable<ClientData> */
{
	private InetSocketAddress address;
	private String nick;
	private int pulsesMissed;
	private int pulseMissAllowance = 3;	// client timeout will be: pulseFrequence * (1 + pulseMissAllowance)
	
	public ClientData(InetSocketAddress address, String nick)
	{
		this.address = address;
		this.nick = nick;
		pulsesMissed = 0;
	}

	// Called by Pulser when a pulse is send
	protected void incrementSentPulses()
	{
		pulsesMissed++;
	}

	// Called by receiver when a ping response (pong) is given by client
	protected void resetPulsesMissed()
	{
		pulsesMissed = 0;
	}


	@Override
	public String toString()
	{
		return "[" + nick + "]";
	}

	
	protected InetSocketAddress getAddress()
	{
		return address;
	}
	
	
	protected String getNick()
	{
		return nick;
	}

	
	protected int getPulsesMissed()
	{
		return pulsesMissed;
	}

	
	protected int getPulseMissAllowance()
	{
		return pulseMissAllowance;
	}

}
