package server;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetSocketAddress;
import java.util.concurrent.ConcurrentHashMap;

/**
 * 
 * @author Illum
 * @since 31-March-2014
 */
public class Pulser implements Runnable
{
	private ConcurrentHashMap<InetSocketAddress, ClientData> joinedClients;
	private DatagramSocket socket;
	private boolean pulseActive;
	private final long pulseFrequence = 5000;
	private DatagramPacket pulsePacket;
	private final String PING_KEYWORD = "PING";
	
	
	protected Pulser(ConcurrentHashMap<InetSocketAddress, ClientData> joinedClients, DatagramSocket socket)
	{
		this.joinedClients = joinedClients;
		this.socket = socket;
		byte[] pulseBytes = PING_KEYWORD.getBytes();
		pulsePacket = new DatagramPacket(pulseBytes, pulseBytes.length);
		pulseActive = true;
	}
	

	public void run()
	{
		try
		{
			checkPulse();
		}
		catch (InterruptedException e)
		{
			e.printStackTrace();
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
	}
	

	private void checkPulse() throws InterruptedException, IOException
	{
		while (pulseActive)
		{	
			System.out.println(joinedClients);
			for (ClientData currentClient: joinedClients.values())
			{
				if (currentClient.getPulsesMissed() <= currentClient.getPulseMissAllowance())
				{
					pulsePacket.setSocketAddress(currentClient.getAddress());
					socket.send(pulsePacket);
					currentClient.incrementSentPulses();
				}
				else
				{
					// removing client
					joinedClients.remove(currentClient.getAddress());
					System.out.println("Should be empty:" + joinedClients);
				}
			}
			Thread.sleep(pulseFrequence);	
		}
	}
	
	
	protected void setPulseActive(boolean pulseActive)
	{
		this.pulseActive = pulseActive;
	}
	
	
}
